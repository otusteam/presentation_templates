class: center, middle, first

<div class="first-page__header">
  <img src="images/logo.svg" class="first-page__logo">
</div>

# TITLE OF YOUR PRESENTATION

<div class="first-page__footer">
  <img src="" class="first-page__footer-image">
</div>

---
class: author-slide

<div class="author">
  <div class="info-box">
    <div class="name">
      Hi there. <br /> I'm Pavel Kedrinskiy
    </div>
    <div class="info">
      <div class="row">
        Technologies: Python, JavaScript, Docker, ML
      </div>
    </div>
  </div>
</div>

<div class="contact-info">
  <div class="info-box">
    <div class="contact-header">
      Contact Information:
    </div>
    <div class="contact-data">
      <div class="row">
        Phone: +375(29) 202 89 18
      </div>
      <div class="row">
        Email: pavel.k.python@gmail.com
      </div>
      <div class="row">
        Skype: pavel776f.726b
      </div>
    </div>
    <p>
    <img src='images/barcode.png'/>
    </p>
  </div>
</div>


???

YOUR NOTES HERE

---
layout: true

<div class="regular-page__background">
  <img src="images/logo.svg" class="regular-page__logo">
</div>

---
class: center, middle

<img src="images/YOUR_IMAGE_HERE.gif" width="800px" />

---
class: xs-code
# YOUR HEADER THERE ABOUT CODE
```python
...
field3 = fields.One2many('res.users')
```
```python
@api.onchange('field1')
def _onchange_field1(self):
    vals = self.env['res.users']
    for i in range(10):
        vals += vals.new({'name':'name{}'.format(i)})
    self.field3 = vals 
```
---

class: s-code
# YOUR HEADER THERE ABOUT CODE
```json
{ 
 "name":"John", 
  "age":30, 
  "car":null
}
```
---
class: center
# YOUR HEADER THERE
<img src="images/YOUR_IMAGE_HERE.gif" height="550px">

???

YOUR NOTES HERE

---
layout: false
background-image: url(/images/Thats_all_folks.svg)
